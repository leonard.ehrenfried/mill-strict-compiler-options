import mill._
import mill.define.Target
import mill.scalalib._
import mill.scalalib.publish.{VersionControl, _}

object VCS {
  def gitlab(owner: String, repo: String, tag: Option[String] = None): VersionControl =
    VersionControl(
      browsableRepository = Some(s"https://gitlab.com/$owner/$repo"),
      connection = Some(VersionControlConnection.gitGit("gitlab.com", s"$owner/$repo.git")),
      developerConnection = Some(VersionControlConnection.gitSsh("gitlab.com", s":$owner/$repo.git", username = Some("git"))),
      tag = tag
    )
}

object `mill-strict-compiler-options` extends ScalaModule with PublishModule {
  def scalaVersion = "2.12.4"
  def publishVersion = "0.0.1"

  override def ivyDeps: Target[Agg[Dep]] = Agg(ivy"com.lihaoyi::mill-scalalib:0.3.5")

  def pomSettings = PomSettings(
    description = "Strict compiler options for scalac",
    organization = "io.leonard",
    url = "https://gitlab.com/lihaoyi/mill",
    licenses = Seq(License.MIT),
    versionControl = VCS.gitlab("leonard.ehrenfried", "mill-strict-compiler-options"),
    developers = Seq(
      Developer("leonard", "Leonard Ehrenfried", "https://gitlab.com/leonard.ehrenfried")
    )
  )
}
